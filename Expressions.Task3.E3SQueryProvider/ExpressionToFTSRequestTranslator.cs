﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Expressions.Task3.E3SQueryProvider
{
    public class ExpressionToFtsRequestTranslator : ExpressionVisitor
    {
        readonly StringBuilder _resultStringBuilder;

        public ExpressionToFtsRequestTranslator()
        {
            _resultStringBuilder = new StringBuilder();
        }

        public string Translate(Expression exp)
        {
            Visit(exp);
            return _resultStringBuilder.ToString();
        }

        #region protected methods

        private void HandleStringMethodCall(MethodCallExpression node)
        {
            Visit(node.Object);
            _resultStringBuilder.Append("(");
            switch (node.Method.Name)
            {
                case nameof(string.Equals):
                    Visit(node.Arguments[0]);
                    break;
                case nameof(string.StartsWith):
                    Visit(node.Arguments[0]);
                    _resultStringBuilder.Append("*");
                    break;
                case nameof(string.EndsWith):
                    _resultStringBuilder.Append("*");
                    Visit(node.Arguments[0]);
                    break;
                case nameof(string.Contains):
                    _resultStringBuilder.Append("*");
                    Visit(node.Arguments[0]);
                    _resultStringBuilder.Append("*");
                    break;
                default: throw new NotSupportedException();
            }
            _resultStringBuilder.Append(")");
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable)
                && node.Method.Name == "Where")
            {
                var predicate = node.Arguments[1];
                Visit(predicate);

                return node;
            }
            else if (node.Method.DeclaringType == typeof(String))
            {
                HandleStringMethodCall(node);
                return node;
            }
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
                    Expression memberAccess = node.Left.NodeType == ExpressionType.MemberAccess ? node.Left : (node.Right.NodeType == ExpressionType.MemberAccess ? node.Right : null);
                    if (memberAccess == null)
                        throw new NotSupportedException($"There must be operand property or field: {node.NodeType}");

                    Expression constant = node.Left.NodeType == ExpressionType.Constant ? node.Left : (node.Right.NodeType == ExpressionType.Constant ? node.Right : null);
                    if (constant == null)
                        throw new NotSupportedException($"There must be constant: {node.NodeType}");

                    Visit(memberAccess);
                    _resultStringBuilder.Append("(");
                    Visit(constant);
                    _resultStringBuilder.Append(")");
                    break;
                case ExpressionType.AndAlso:
                    Visit(node.Left);
                    _resultStringBuilder.Append("&&");
                    Visit(node.Right);
                    break;
                default:
                    throw new NotSupportedException($"Operation '{node.NodeType}' is not supported");
            };

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            _resultStringBuilder.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            _resultStringBuilder.Append(node.Value);

            return node;
        }

        #endregion
    }
}
